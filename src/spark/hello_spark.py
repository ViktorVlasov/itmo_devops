# Import the necessary modules
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

conf = SparkConf().setAppName("Hello Spark").setMaster("spark://spark-master:7077")
sc = SparkContext(conf=conf)

# Доступ к логгеру Spark
log4jLogger = sc._jvm.org.apache.log4j
LOGGER = log4jLogger.LogManager.getLogger(__name__)

LOGGER.info("Starting the Spark Application")
# Start the SparkSession
spark = SparkSession.builder \
                    .config(conf=conf) \
                    .getOrCreate()

rdd = sc.parallelize(range(1, 100))

LOGGER.info(f"THE SUM IS HERE: {rdd.sum()}")
LOGGER.info("Stopping the Spark Application")
# Stop the SparkSession
spark.stop()

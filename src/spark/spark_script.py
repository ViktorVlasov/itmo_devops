# from pyspark import SparkConf, SparkContext
# from pyspark.sql import SparkSession
# import sys

# conf = SparkConf().setAppName("My PySpark App").setMaster("spark://spark-master:7077")
# sc = SparkContext(conf=conf)

# spark = SparkSession.builder \
#     .config(conf=conf) \
#     .getOrCreate()

# input_filepath = sys.argv[1] # получаем путь к csv файлу
# print(input_filepath)
# df = spark.read.csv(input_filepath, header=True, sep='\t')

# # Заполняем пропуски в столбце work_skills пустыми строками
# df = df.fillna({'work_skills': ''})

# # Сохраняем DataFrame в CSV с новым префиксом
# output_filepath = input_filepath.replace("transformed_data", "transformed_data_fill_na")
# df.write.csv(output_filepath, header=True, sep='\t', index=False, na_rep='\\N')

# # Stop the SparkSession
# spark.stop()


# Import the necessary modules
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
import sys
import os

conf = SparkConf().setAppName("My PySpark App").setMaster("spark://spark-master:7077")
sc = SparkContext(conf=conf)
filename = sys.argv[1]
input_filepath = f"/opt/spark/data_tmp/{filename}" # получаем путь к csv файлу

# if os.path.exists(input_filepath):
#     print(f"File {input_filepath} exists!")
# else:
#     print(f"File {input_filepath} does not exist!")
    
# input_filepath = f"/opt/airflow/data_tmp/{filename}" # получаем путь к csv файлу
# if os.path.exists(input_filepath):
#     print(f"File {input_filepath} exists!")
# else:
#     print(f"File {input_filepath} does not exist!")


# Start the SparkSession
spark = SparkSession.builder \
                    .config(conf=conf) \
                    .getOrCreate()

df = spark.read.csv(input_filepath, header=True, sep='\t')

# Заполняем пропуски в столбце work_skills пустыми строками
df = df.fillna({'work_skills': ''})

output_filepath = input_filepath.replace("transformed_data", "transformed_data_fill_na")
df.write.csv(output_filepath, header=True, sep='\t', index=False, na_rep='\\N')

# Stop the SparkSession
spark.stop()

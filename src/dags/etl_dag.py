from datetime import datetime, timedelta
import pendulum
from airflow.decorators import task, dag
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
# from airflow.providers.postgres.hooks.postgres import PostgresHook
# from airflow.models import Variable
import logging

task_logger = logging.getLogger("airflow.task")

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2023, 10, 25, tzinfo=pendulum.timezone("UTC")),
    'retries': 1
}

@dag(default_args=default_args, schedule_interval='@daily', catchup=False, 
     description='ET(L) pipeline for fetching API data, transforming it and loading to Postgres')
def etl_dag():

    def get_previous_day_dates(date_start: str) -> (str, str):
        date_start = datetime.strptime(date_start, '%Y-%m-%d').date()
        prev_day_start = date_start - timedelta(days=1)
        dateFrom = prev_day_start.strftime('%Y-%m-%dT00:00:00Z')
        dateTo = prev_day_start.strftime('%Y-%m-%dT23:59:59Z')
        return dateFrom, dateTo

    @task()
    def extract(date_start: str):
        import json
        import requests

        BASE_URL = "http://opendata.trudvsem.ru/api/v1/vacancies"
        LIMIT = 100
        STATUS_OK = '200'
        EMPTY_RESULT = 0
        MAX_OFFSET = 10 # int(Variable.get("MAX_OFFSET"))

        dateFrom, dateTo = get_previous_day_dates(date_start)
        combined_vacancies = []

        params = {
            "modifiedFrom": dateFrom,
            "modifiedTo": dateTo,
            "limit": LIMIT
        }

        for offset in range(1, MAX_OFFSET):
            params['offset'] = offset

            try:
                r = requests.get(BASE_URL, params=params, timeout=60)
                json_data = r.json()
            except (requests.Timeout, requests.ConnectionError) as e:
                task_logger.error(f"Error while fetching data: {e}")
                continue

            if json_data['status'] != STATUS_OK or \
                json_data['meta']['total'] == EMPTY_RESULT:
                break

            combined_vacancies.extend(json_data['results']['vacancies'])

        task_logger.info("Extract task is OK")
        return combined_vacancies


    @task()
    def base_transform(vacancies, date_start: str):
        import pandas as pd
        import numpy as np
        
        ONLY_PREV_DAY = False #int(Variable.get('ONLY_PREV_DAY'))

        def extract_from_vacancy(vacancy, *keys, default=np.nan):
            """
            Получение значения из вакансии по цепочке ключей или 
            возвращение значения по умолчанию.
            """
            try:
                for key in keys:
                    vacancy = vacancy[key]
                return vacancy
            except (KeyError, IndexError, TypeError):
                return default

        data = [{
            'id': extract_from_vacancy(vacancy, 'vacancy', 'id'),
            'custom_position': extract_from_vacancy(vacancy, 'vacancy', 'job-name'),
            'schedule': extract_from_vacancy(vacancy, 'vacancy', 'schedule'),
            'salary_from': extract_from_vacancy(vacancy, 'vacancy', 'salary_min'),
            'salary_to': extract_from_vacancy(vacancy, 'vacancy', 'salary_max'),
            'education_name': extract_from_vacancy(vacancy, 'vacancy', 'requirement', 'education'),
            'region_name': extract_from_vacancy(vacancy, 'vacancy', 'region', 'name'),
            'location': extract_from_vacancy(vacancy, 'vacancy', 'addresses', 'address', 0, 'location'),
            'creation_date': extract_from_vacancy(vacancy, 'vacancy', 'creation-date'),
            'work_skills': extract_from_vacancy(vacancy, 'vacancy', 'duty'),
            'experience': extract_from_vacancy(vacancy, 'vacancy', 'requirement', 'experience')
        } for vacancy in vacancies]

        df = pd.DataFrame(data)

        if ONLY_PREV_DAY:
            date_start = datetime.strptime(date_start, '%Y-%m-%d').date()
            prev_day = (date_start - timedelta(days=1)).strftime('%Y-%m-%d')
            df = df[df['creation_date'] == prev_day]

        df['experience'] = df['experience'].astype(str)
        
        filename = f'transformed_data_{date_start}.csv'
        filepath = f"/opt/airflow/data_tmp/{filename}"
        df.to_csv(filepath, index=False, sep='\t', na_rep='\\N')
    
        return filename

    data = extract(date_start='{{ ds }}')
    transformed_data = base_transform(data, date_start='{{ ds }}')

    # spark_job = SparkSubmitOperator(
    #     task_id='spark_submit_job',
    #     application="/opt/airflow/spark/spark_script.py",
    #     conn_id="spark_local",
    #     name="spark-job",
    #     verbose=False,
    #     application_args=["{{ ti.xcom_pull(task_ids='base_transform') }}"],  # Получение результата из base_transform
    # )
    # spark_job = SparkSubmitOperator(task_id='test',
    #                             application=f'/opt/airflow/spark/spark_script.py',
    #                             name='test',
    #                             conn_id='spark_local',
    #                             application_args=["{{ ti.xcom_pull(task_ids='base_transform') }}"])


    # transformed_data >> spark_job


dag_instance = etl_dag()

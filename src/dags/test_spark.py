from datetime import datetime, timedelta
from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.models import Variable

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(
    'test_spark',
    default_args=default_args,
    description='A simple DAG for testing spark',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2023, 9, 27),
    catchup=False
)

spark_job = SparkSubmitOperator(task_id='test',
                                application=f'/opt/airflow/spark/hello_spark.py',
                                name='test',
                                conn_id='spark_local',
                                dag=dag)

spark_job

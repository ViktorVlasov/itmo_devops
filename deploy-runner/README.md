#### 1. Запуск в терминале:

В терминале выполнить команду:
```bash
docker-compose up -d
```

#### 2. Получение токена регистрации в GitLab:

- В GitLab, в нужном проекте, перейти в раздел `Settings` -> `CI/CD`.
- Раскрыть пункт `Runners`.
- Скопировать `registration token`.

#### 3. В терминале выполнить команду для инициализации раннера:

```bash
docker exec -it gitlab-runner gitlab-runner register --url "url_гитлаба" --registration-token "registration_token"
```

- **URL:** вставить нужный - [https://gitlab.com/](https://gitlab.com/) для публичного Гитлаба или другой, если используется другой экземпляр.
- **Description:** имя раннера, можно любое (для удобной отладки).
- Вставить токен, скопированный ранее.
- тег придумать сложнее (например `docker_0910`), чтобы не совпало с дефолтными раннерами Гитлаба.
- **maintenance note:** оставить пустым.
- **тип раннера:** выбрать `docker`.
- **Дефолтный образ:** `docker:stable`.

### 4. Настройка после инициализации раннера:

После первичной инициализации раннера, необходимо его донастроить:
- Залогиниться внутрь контейнера командой:
    ```bash
    docker exec -it gitlab-runner bash
    ```
- Выполнить:
    ```bash
    echo '    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]' >> /etc/gitlab-runner/config.toml
    ```
    
- Выполнить:
    ```bash
    docker restart gitlab-runner
    ```

### 5. Проверка в веб-интерфейсе:
Вернуться в веб-интерфейс Gitlab и убедиться, что раннер "поделился" (внизу в разделе Assigned project runners).


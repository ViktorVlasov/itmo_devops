# Changelog

## [0.3.0] - lab3

### Added
- Файл .gitlab-ci.yml
- Стейдж test, правила для шага деплоя и билда
- Директория deploy-runner для поднятия раннера в докер-контейнере
- Описание README.md в директории deploy-runner с инструкцией по запуску раннера

## [0.2.0] - lab2

### Added
- В Docker-compose добавлены два сервиса, отвечающие за Spark (spark-master и spark-worker).
- Добавлен DAG для тестирования Spark и Airflow
- Добавлен скрипт для тестирования Spark, использующий pyspark (SparkSession)
- Добавлен CHANGES.md

### Changed
- Обновлен README, добавлен раздел с подготовкой подключения Spark в Airflow.
- Обновлена структура проекта, директории dags, spark и файлы Dockerfile и requirements.txt перемещены в папку src

## [0.1.0] - lab1

### Added
- Docker-compose и Dockerfile для поднятия сервиса Airflow.
- ETL pipeline (DAG) в Airflow. Извлекает данные из API trudvsem,  делает базовую предобработку и сохраняет csv датасет на диске.
- requirements.txt для задач в DAGe ETL pipeline.

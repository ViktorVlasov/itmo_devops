FROM apache/airflow:2.7.1

WORKDIR /opt/airflow

USER root
RUN apt update && apt -y install procps default-jre

USER airflow
COPY ./src/* ./
# COPY ./src/ ./spark/
# COPY ./src/ ./requirements.txt

RUN pip install -r ./requirements.txt
# Лабораторные работы для курса DevOps в ИТМО

## Описание

Проект предоставляет инструменты для работы с Airflow, Spark в контексте курса DevOps в ИТМО.

## Структура Проекта

- **data_tmp** - Директория для хранения csv датасета, который создается DAGом "etl_dag.py"
- **imgs** - Изображения для README.md
- **src**
  - **dags** - Директория с DAGами
    - etl_dag.py - ETL pipeline для получения датасета с вакансиями за 1 день с помощью API trudvsem.
    - test_spark.py - DAG для тестирования Spark
  - **spark** - Директория с spark-job
  - **Dockerfile** - Dockerfile с образом Airflow
  - **requirements.txt** - зависимости для выполнения DAGов
- **docker-compose.yaml** - сервисы Airflow-Postgres-Spark
- **.gitignore**
- **CHANGES.md**
- **README.md**


## :ship: Containers

* **postgres**: Postgres database (Airflow metadata)
    * image: postgres:13
    * port: 5432

* **airflow-webserver**: Airflow v2.7.1 (Webserver & Scheduler)
    * image: Custom, based on apache/airflow:2.7.1
    * port: 8080 

* **spark-master**: Spark Master
    * image: apache/spark:latest
    * port: 4040, 7077

* **spark-worker**: Spark workers
    * image: apache/spark:latest
    * port: 7001


## How to use

### Минимальные требования

- Docker CE>=19.03
- RAM > 4 GB

### Шаги по установке проекта

1. Клонировать репозиторий:
   ```bash
   git clone https://gitlab.com/ViktorVlasov/itmo_devops.git
    ```

2. Запустить Docker Compose:
   ```bash
   docker-compose up -d
    ```

3. Войти в Airflow:
   - Login: airflow
   - Password: airflow

4. В админке Airflow требуется руками создать подключение к кластеру Spark (Admin → Connections → + New)
   ![](./imgs/connections.png "connections")